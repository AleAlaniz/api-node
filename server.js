const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const server = require('http').Server(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/iecho',(req,res)=>{
       let text,
       response = {};
       res.setHeader('Content-Type','application/json');
       
       text = req.param("text");
       if(text){
              if(process.argv[2])
              {
                     text = process.argv[2];
              }
              let reversed = text.split("").reverse().join("");
              // res.status(200).send(reversed);
              response = {
                     "text": reversed
              }
              res.status(200);
              
       }
       else{
              res.status(400);
              response = {
                     "error": "no text"
              }
       }
       
       res.end(JSON.stringify(response));
})

server.listen(3000,(err)=>{
       if(err){
              throw err;
       }
       console.log("corriendo en puerto 3000");
})